import webApp

formularioPOST = '<form action="" method="post" class="form-example" ' \
                 + '<div class="form-example">' + '<label for="recurso">Introduce resource here: ' \
                 + '</label>' + '<input type="text" name="resource" id="resource" required>' \
                 + '</div>' + '<div class="form-example">' + '<input type="submit" value="Sent!">' \
                 + '</div></form>'


class contentPostApp(webApp.webApp):

    resources = {'/': 'p&aacute;gina principal',
               '/ruben':'p&aacute;gina de ruben'}

    def parse(self, received):
        recibido = received.decode()
        method = recibido.split(' ')[0]
        resource = recibido.split(' ')[1]
        if method == "POST":
            body = recibido.split('\r\n\r\n')[1]
        else:
            body = None

        return method, resource, body

    def process(self, analyzed):

        method, resource, body = analyzed

        if method == "POST":
            self.resources[resource] = body
            http = "200 OK"
            html = "<html><body><h4>Introduce content</h4>Resource requested: " \
                   + resource + "<br>Content resource: " + self.resources[resource] \
                   + formularioPOST + "</body></html>"
        elif method == "PUT":
            self.resources[resource] = body
            http = "200 OK"
            html = "<html><body>Content added successfully</body></html>"
        elif resource in self.resources.keys():
            http = "200 OK"
            html = "<html><body><h4>P&aacute;gina encontrada</h4>resource requested is  : " \
                   + resource + "<br>" + "Content: " + self.resources[resource] \
                   + "<p>Introduce new resource: </p>" + formularioPOST + "</body></html>"
        else:
            http = "404 NOT FOUND"
            html = "<html><body><h1>404 ERROR NOT FOUND" + formularioPOST + "</h1></body></html>"
        return http, html


if __name__ == "__main__":
    contentPostAPP= contentPostApp('localhost', 1234)